CREATE TABLE IF NOT EXISTS Users (
  id       SERIAL PRIMARY KEY,
  login    TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS Results (
  id      SERIAL PRIMARY KEY,
  user_id SERIAL REFERENCES Users (id) NOT NULL,
  x       TEXT                         NOT NULL,
  y       TEXT                         NOT NULL,
  r       TEXT                         NOT NULL,
  in_area TEXT                         NOT NULL
);