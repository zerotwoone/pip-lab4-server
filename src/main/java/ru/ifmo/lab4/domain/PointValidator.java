package ru.ifmo.lab4.domain;

public interface PointValidator {
    /**
     * Проверяет вхождение точки в область.
     * @param model Точка для проверки
     * @return Входит ли точка в область
     */
    ResultModel validatePoint(PointModel model);
}
