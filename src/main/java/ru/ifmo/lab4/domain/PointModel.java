package ru.ifmo.lab4.domain;

import java.math.BigDecimal;

public class PointModel {
    private final BigDecimal x;
    private final BigDecimal y;
    private final BigDecimal r;

    public PointModel(BigDecimal x, BigDecimal y, BigDecimal r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public BigDecimal getX() {
        return x;
    }

    public BigDecimal getY() {
        return y;
    }

    public BigDecimal getR() {
        return r;
    }
}
