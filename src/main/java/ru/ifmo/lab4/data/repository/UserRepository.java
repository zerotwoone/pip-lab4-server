package ru.ifmo.lab4.data.repository;

import org.springframework.data.repository.CrudRepository;
import ru.ifmo.lab4.data.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    UserEntity findByLogin(String login);
}
